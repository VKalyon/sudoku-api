﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sudoku_API.Models
{
    public class UserArgs
    {
        public string EmailAddress { get; set; }
        public string Password { get; set; }
    }
}
