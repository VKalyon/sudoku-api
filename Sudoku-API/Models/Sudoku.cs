﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Sudoku_API.Models
{
    public class Sudoku
    {
        [Key]
        public int SudokuID { get; set; }
        [Required]
        public string SudokuGiven { get; set; }
        [Required]
        public string SudokuAnswer { get; set; }
    }
}
