﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sudoku_API.Models
{
    public class UserSudokuArgs
    {
        public bool Status { get; set; }
        public double? CompletionTime { get; set; }
        public int UserId { get; set; }
        public int SudokuID { get; set; }
    }
}
