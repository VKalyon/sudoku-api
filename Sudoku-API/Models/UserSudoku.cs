﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Sudoku_API.Models
{
    public enum SudokuStatus
    {
        Started,
        Finished
    }

    public class UserSudoku
    {
        [Key]
        public int UserSudokuID { get; set; }       
        public SudokuStatus Status { get; set; }
        public double? CompletionTime { get; set; }

        [ForeignKey("User")]
        public int UserID { get; set; }
        public User User { get; set; }


        [ForeignKey("Sudoku")]
        public int SudokuID { get; set; }
        public Sudoku Sudoku { get; set; }
    }
}
