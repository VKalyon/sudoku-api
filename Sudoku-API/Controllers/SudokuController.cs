﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LumenWorks.Framework.IO.Csv;
using System.IO;
using System.Data;
using Sudoku_API.Models;
using Sudoku_API.Contexts;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Sudoku_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SudokuController : ControllerBase
    {
        private readonly SudokuAppContext context;

        public SudokuController()
        {
            context = new SudokuAppContext();
        }

        // GET: api/<SudokuController>
        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                var sudokus = context.Sudokus;
                return Ok(sudokus);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.GetType());
                return BadRequest();
            }
        }

        // GET api/<SudokuController>/5
        [HttpGet("random")]
        public IActionResult Get(string random)
        {
            int maxSudokus = context.Sudokus.OrderByDescending(s => s.SudokuID).First().SudokuID;

            Random randomSudoku = new Random();
            int getRandom = randomSudoku.Next(0, maxSudokus);

            try
            {
                Sudoku sudoku = context.Sudokus.Where(s => s.SudokuID == getRandom).FirstOrDefault();
                if (sudoku == null) return NotFound();
                return Ok(sudoku);
            }
            catch
            {
                return BadRequest();
            }
        }

        // Method to add sudoku puzzles from a CSV file
        private void UploadCSVContents(string filePath)
        {
            List<Sudoku> sudokus = new List<Sudoku>();
            var csvTable = new DataTable();

            using (var csvReader = new CsvReader(new StreamReader(System.IO.File.OpenRead(filePath)), true))
            {
                csvTable.Load(csvReader);
            }

            for (int i = 0; i < csvTable.Rows.Count; i++)
            {
                sudokus.Add(new Sudoku { 
                    SudokuGiven = csvTable.Rows[i][0].ToString(),
                    SudokuAnswer = csvTable.Rows[i][1].ToString()
                });
            }

            foreach(Sudoku sudoku in sudokus)
            {
                context.Sudokus.Add(sudoku);
            }

            context.SaveChanges();
        }

        //// POST api/<SudokuController>
        //[HttpPost]
        //public void Post([FromBody] string value)
        //{
        //}

        //// PUT api/<SudokuController>/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody] string value)
        //{
        //}

        //// DELETE api/<SudokuController>/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
    }
}
