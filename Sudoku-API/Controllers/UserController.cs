﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Sudoku_API.Contexts;
using Sudoku_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sudoku_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly SudokuAppContext context;

        public UserController()
        {
            context = new SudokuAppContext();
        }

        [HttpGet]
        public IActionResult Get()
        {
            var users = context.Users;
            return Ok(users);
        }

        [HttpPost]
        public IActionResult Post()
        {
            User user = new User(){EmailAddress = "test@test.test", Password = "password123"};
            context.Users.Add(user);
            context.SaveChanges();

            return Ok();
        }
    }
}
