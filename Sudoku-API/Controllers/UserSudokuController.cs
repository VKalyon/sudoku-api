﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Sudoku_API.Contexts;
using Sudoku_API.Hubs;
using Sudoku_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Sudoku_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserSudokuController : ControllerBase
    {
        private readonly UserSudokuContext _context;
        private readonly IHubContext<LeaderboardHub> _hubContext;

        public UserSudokuController(IHubContext<LeaderboardHub> hubContext)
        {
            _context = new UserSudokuContext();
            _hubContext = hubContext;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] UserSudokuArgs _args)
        {
            UserSudoku userSudoku = new UserSudoku()
            {
                UserID = _args.UserId,
                SudokuID = _args.SudokuID,
                CompletionTime = _args.CompletionTime,
                Status = SudokuStatus.Finished
            };
            var saveResult = await _context.SaveUserSudokuAsync(userSudoku);

            if(saveResult)
            {
                var topTen = _context.LeaderboardTopTen();
                await _hubContext.Clients.All.SendAsync("refreshLeaderboard", topTen);
            }

            return Ok();
        }
    }
}
