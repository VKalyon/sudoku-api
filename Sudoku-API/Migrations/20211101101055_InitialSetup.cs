﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sudoku_API.Migrations
{
    public partial class InitialSetup : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Sudoku",
                columns: table => new
                {
                    SudokuID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SudokuGiven = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    SudokuAnswer = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sudoku", x => x.SudokuID);
                });

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    UserID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EmailAddress = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Password = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.UserID);
                });

            migrationBuilder.CreateTable(
                name: "UserSudoku",
                columns: table => new
                {
                    UserSudokuID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Status = table.Column<int>(type: "int", nullable: false),
                    CompletionTime = table.Column<int>(type: "int", nullable: true),
                    UserID = table.Column<int>(type: "int", nullable: false),
                    SudokuID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserSudoku", x => x.UserSudokuID);
                    table.ForeignKey(
                        name: "FK_UserSudoku_Sudoku_SudokuID",
                        column: x => x.SudokuID,
                        principalTable: "Sudoku",
                        principalColumn: "SudokuID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserSudoku_User_UserID",
                        column: x => x.UserID,
                        principalTable: "User",
                        principalColumn: "UserID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UserSudoku_SudokuID",
                table: "UserSudoku",
                column: "SudokuID");

            migrationBuilder.CreateIndex(
                name: "IX_UserSudoku_UserID",
                table: "UserSudoku",
                column: "UserID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserSudoku");

            migrationBuilder.DropTable(
                name: "Sudoku");

            migrationBuilder.DropTable(
                name: "User");
        }
    }
}
