﻿using Microsoft.EntityFrameworkCore;
using Sudoku_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sudoku_API.Contexts
{
    public class SudokuAppContext : DbContext
    {        
        //public SudokuAppContext(DbContextOptions<SudokuAppContext> options) : base(options)
        //{
        //}

        public DbSet<User> Users { get; set; }
        public DbSet<Sudoku> Sudokus { get; set; }
        public DbSet<UserSudoku> UserSudokus { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=mssql.fhict.local;Database=dbi441516_sudokuapp;User Id=dbi441516_sudokuapp;Password=sudoku;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().ToTable("User");
            modelBuilder.Entity<Sudoku>().ToTable("Sudoku");            
            modelBuilder.Entity<UserSudoku>().ToTable("UserSudoku");
        }
    }
}
