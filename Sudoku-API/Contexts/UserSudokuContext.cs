﻿using Sudoku_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sudoku_API.Contexts
{
    public class UserSudokuContext: SudokuAppContext
    {

        public async Task<bool> SaveUserSudokuAsync(UserSudoku _userSudoku)
        {
            try
            {
                UserSudokus.Add(_userSudoku);

                return await SaveChangesAsync() > 0;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<UserSudoku> LeaderboardTopTen()
        {
            return UserSudokus.OrderBy(us => us.CompletionTime).Take(10);
        }
    }
}
